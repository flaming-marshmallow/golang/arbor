package arbor

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestFoo(t *testing.T) {
	a := 3

	assert.Equal(t, a, 3)
}
