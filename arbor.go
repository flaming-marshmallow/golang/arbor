package arbor

import (
	"context"
	"sync"
	"time"

	"gitlab.com/flaming-marshmallow/golang/arbor/internal/cache"
)

//https://guava.dev/releases/21.0/api/docs/com/google/common/cache/Cache.html

// var _ Cache[string, any] = (*cache.SimpleCache[string, any])(nil)

//TODO any returned data CAN NOT be used to modify cache data

type Cache[K comparable, V any] interface {
	AsMap() map[K]V //concurrentMap
	CleanUp()
	//guava#loadingCache.get(key)
	Get(key K) (V, error)
	//guava#cache.get(key, loader)
	GetAndLoad(key K, loader func() (V, error)) (V, error)
	GetAllPresent(keys []K) (map[K]V, error)
	GetIfPresent(key K) V
	Invalidate(key K)
	InvalidateAll()
	Put(key K, value V)
	PutAll(m map[K]V)
	Size() int64
	// Stats() CacheStats
}

type threadSafeCache[K comparable, V any] struct {
	cache cache.Cache[K, V]
	mu    sync.Mutex
}

// AsMap implements Cache.
func (tsc *threadSafeCache[K, V]) AsMap() map[K]V {
	tsc.mu.Lock()
	defer tsc.mu.Unlock()

	return tsc.cache.AsMap()
}

// CleanUp implements Cache.
func (tsc *threadSafeCache[K, V]) CleanUp() {
	tsc.mu.Lock()
	defer tsc.mu.Unlock()

	tsc.cache.CleanUp()
}

// Get implements Cache.
func (tsc *threadSafeCache[K, V]) Get(key K) (V, error) {
	tsc.mu.Lock()
	defer tsc.mu.Unlock()

	return tsc.cache.Get(key)
}

// GetAllPresent implements Cache.
func (*threadSafeCache[K, V]) GetAllPresent(keys []K) (map[K]V, error) {
	panic("unimplemented")
}

// GetAndLoad implements Cache.
func (*threadSafeCache[K, V]) GetAndLoad(key K, loader func() (V, error)) (V, error) {
	panic("unimplemented")
}

// GetIfPresent implements Cache.
func (*threadSafeCache[K, V]) GetIfPresent(key K) V {
	panic("unimplemented")
}

// Invalidate implements Cache.
func (*threadSafeCache[K, V]) Invalidate(key K) {
	panic("unimplemented")
}

// InvalidateAll implements Cache.
func (*threadSafeCache[K, V]) InvalidateAll() {
	panic("unimplemented")
}

// Put implements Cache.
func (*threadSafeCache[K, V]) Put(key K, value V) {
	panic("unimplemented")
}

// PutAll implements Cache.
func (*threadSafeCache[K, V]) PutAll(m map[K]V) {
	panic("unimplemented")
}

// Size implements Cache.
func (*threadSafeCache[K, V]) Size() int64 {
	panic("unimplemented")
}

type CacheBuilder[K comparable, V any] struct {
	initialSize      int64
	maxSize          int64
	expireAfterWrite time.Duration
	loader           func(key K) (V, error)
	statsRecorder    cache.StatsRecorder
	evictionHandler  func(key K, value V)
}

func (cb *CacheBuilder[K, V]) MaximumSize(maxSize int64) CacheBuilder[K, V] {
	cb.maxSize = maxSize
	return *cb
}

func (cb *CacheBuilder[K, V]) ExpireAfterWrite(duration time.Duration) CacheBuilder[K, V] {
	cb.expireAfterWrite = duration
	return *cb
}

func (cb *CacheBuilder[K, V]) WithLoader(loader func(K) (V, error)) CacheBuilder[K, V] {
	cb.loader = loader
	return *cb
}

func (cb *CacheBuilder[K, V]) RecordStats() CacheBuilder[K, V] {
	cb.statsRecorder = *new(cache.StatsRecorder) //cache.NewStatsRecorder()
	return *cb
}

func (cb *CacheBuilder[K, V]) WithStatsRecorder(recorder cache.StatsRecorder) CacheBuilder[K, V] {
	cb.statsRecorder = recorder
	return *cb
}

func (cb *CacheBuilder[K, V]) OnEvict(evictionHandler func(K, V)) CacheBuilder[K, V] {
	cb.evictionHandler = evictionHandler
	return *cb
}

func (cb *CacheBuilder[K, V]) Build() Cache[K, V] {
	if cb.loader == nil {
		cb.loader = func(key K) (V, error) {
			return *new(V), nil
		}
	}
	if cb.statsRecorder == nil {
		cb.statsRecorder = *new(cache.StatsRecorder) //newNopRecorder()
	}

	config := cache.CacheConfig[K, V]{
		InitialCapacity: int(cb.initialSize),
		Loader:          cb.loader,
		StatsRecorder:   cb.statsRecorder,
		EvictionHandler: cb.evictionHandler,
	}

	newCache := threadSafeCache[K, V]{
		cache: cache.New(config, context.Background()),
	}

	return &newCache
}
