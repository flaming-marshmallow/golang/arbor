
# Arbor Key Value Store


[![Pipeline Status](https://gitlab.com/flaming-marshmallow/golang/arbor/badges/main/pipeline.svg")](https://gitlab.com/flaming-marshmallow/golang/arbor/-/pipelines)
<img src="https://gitlab.com/flaming-marshmallow/golang/arbor/badges/main/coverage.svg" />
<img src="https://gitlab.com/flaming-marshmallow/golang/arbor/-/badges/release.svg" />

contributers:1 | commit-activity:3/month | build:passing | tests:passing | coverage:88% | chat:1 online
version:1.2.3 | license:bsd-3 | types:go



-    %{project_path}: Path of a project including the parent groups
-    %{project_title}: Title of a project
-    %{project_name}: Name of a project
-    %{project_id}: Database ID associated with a project
-    %{project_namespace}: Project namespace of a project
-    %{group_name}: Group of a project
-    %{gitlab_server}: Server of a project
-    %{gitlab_pages_domain}: Domain of a project
-    %{default_branch}: Default branch name configured for a project’s repository
-    %{commit_sha}: ID of the most recent commit to the default branch of a project’s repository
-    %{latest_tag}: Latest tag added to the project’s repository 


## Introduction

A thread-safe, distributed, peer to peer caching library.

## Goals

1. cache similar to guava
2. peer-2-peer cache
3. eventually consistent
4. grpc for p2p messaging

## Builds

Build all of the commands
`go build -o bin/ ./cmd/...`


## TODO

- [x] buffer
- [x] define cache api
- [x] determine if the cache will be LRU or LFU
- [x] define loader interface
- [ ] implement opentelemetry for cache stats
- [ ] cache stats api similar to guava
- [x] linked hashmap
- [ ] cache reaper thread for evictions (expired entries)
- [ ] cache has channel for sending updates and evictions for sending to peers
- [ ] p2p messaging
- [ ] cache must be thread safe
- [ ] move internal/buffer to the util repo
- [ ] move internal/list to the util repo
- [ ] add code of conduct from https://github.com/avelino/awesome-go?tab=coc-ov-file

## Things to Think About
- [ ] Incorporate redis features like scoring?
- [ ] grpc vs http
- [ ] protobuf? avro? just serialize to binary?
- [ ] Can the peers be leaderless?
- [ ] mechanism for registering new peers.  zookeeper?
- [ ] send data to other nodes


## Ideas To Work Out

- standard local buffer if no peers are defined
- can recieve explicit peer IP
- zookeeper peer discovery
  - register node IP
  - query for peer IPs
  - periodic updates of peers
- update myself, then update network
- add a key-value store mode (aka in-memory redis) with or without eviction
- can we just distribute entries to the protected portion of the segmented-LRU cache?


### protected/probation segment methods

- protected up to 80% of capacity, then evict to probation
- probation can grow up to 100% of capacity
  - if cache is full, addition to probation forces an eviction from protected, which can spiral to force an eviction from probation
  - full cache could creat 1 insert and 2 evictions (protected followed by probation)

```
prot[] -> []   -> []     -> [b]  -> [b]
prob[] -> [a]  -> [b,a]  -> [a]  -> [c,a]
       add(a)  add(b)    get(b)  -> add(c)

cap 10 (protected capped at 80% of capacity)
start                  add(k)             get(i)               get(j)               add(l)
prot[a,b,c,d,e,f,g,h]  -> [a,b,c,d,e,f,g] -> [i,a,b,c,d,e,f,g] -> [j,i,a,b,c,d,e,f] -> [j,i,a,b,c,d,e]
prob[i,j]              -> [k,i,j]         -> [k,j]             -> [g,k]             -> [f,l,g] 
                                                                  g->prob              f->prob
                                                                                       k->evict


```



>From the documentation..
>
>Cache statistics are incremented according to the following rules:
>
>    When a cache lookup encounters an existing cache entry hitCount is incremented.
>
>    When a cache lookup first encounters a missing cache entry, a new entry is loaded.
>
>        After successfully loading an entry missCount and loadSuccessCount
>        are incremented, and the total loading time, in nanoseconds, is added to totalLoadTime.
>
>        When an exception is thrown while loading an entry, missCount and loadExceptionCount are incremented, and the total loading time, in nanoseconds, is added to totalLoadTime.
>
>        Cache lookups that encounter a missing cache entry that is still loading will wait for loading to complete (whether successful or not) and then increment missCount.
>
>    When an entry is evicted from the cache, evictionCount is incremented.
>
>    No stats are modified when a cache entry is invalidated or manually removed.
>
>    No stats are modified by operations invoked on the asMap view of the cache

https://stackoverflow.com/a/42659065/91336















```
Eviction Policy
- LRU


Cache Writes through channel



https://highscalability.com/design-of-a-modern-cache/


                                                              |P|P|P|P|P|P|...|p|p|p|
---> LRU ---> filter ---> segmented LRU [protected ..... probation] ---> eviction

Admission Window
- popularity sketch ---> CountMin Sketch
- build up popularity before filtering
- aging process to periodically half all the counters

Long Term Retention
- entry starts in probation and subsequent access it is promoted to protected
- when protected is full it evicts to probationary, which may trigger a probationary entry to be evicted

- scavenger thread to sweep the cache and reclaim expired entries
  - just sweep the entire queue
  - time to live (TTL) (write order)
  - time to idle (access Order)

- writes via commit log (write to channel)

```



## Testing

### Benchmarks

```
$ go test -bench . -benchmem -count 10 > 10_runs_bench.txt
$ go test -bench . -benchmem -count 20 > 20_runs_bench.txt

$ benchstat 10_runs_bench.txt 20_runs_bench.txt 
goos: linux
goarch: amd64
pkg: gitlab.com/flaming-marshmallow/golang/arbor/internal/buffer
cpu: AMD Ryzen 7 1700 Eight-Core Processor          
                          │ 10_runs_bench.txt │             20_runs_bench.txt             │
                          │      sec/op       │     sec/op       vs base                  │
_Buffer_Push-16               0.004867n ± 14%   0.004569n ± 46%       ~ (p=0.165 n=10+20)
_ConcurrentBuffer_Push-16     0.005309n ± 38%   0.005562n ± 42%       ~ (p=0.559 n=10+20)
geomean                       0.005083n         0.005041n        -0.83%

                          │ 10_runs_bench.txt │           20_runs_bench.txt            │
                          │       B/op        │    B/op     vs base                    │
_Buffer_Push-16                  0.000 ± 0%     0.000 ± 0%       ~ (p=1.000 n=10+20) ¹
_ConcurrentBuffer_Push-16        0.000 ± 0%     0.000 ± 0%       ~ (p=1.000 n=10+20) ¹
geomean                                     ²               +0.00%                   ²
¹ all samples are equal
² summaries must be >0 to compute geomean

                          │ 10_runs_bench.txt │           20_runs_bench.txt            │
                          │     allocs/op     │ allocs/op   vs base                    │
_Buffer_Push-16                  0.000 ± 0%     0.000 ± 0%       ~ (p=1.000 n=10+20) ¹
_ConcurrentBuffer_Push-16        0.000 ± 0%     0.000 ± 0%       ~ (p=1.000 n=10+20) ¹
geomean                                     ²               +0.00%                   ²
¹ all samples are equal
² summaries must be >0 to compute geomean

```



```c++
int circular_buf_peek(cbuf_handle_t cbuf, uint8_t* data, unsigned int look_ahead_counter) {
int r = -1;
size_t pos;

assert(cbuf && data && cbuf->buffer);
//We can't look beyond the current buffer size
if(circular_buf_empty(cbuf) || look_ahead_counter > circular_buf_size(cbuf)) {
    return r;
}
pos = cbuf->tail;
for (unsigned int i=0; i < look_ahead_counter; i++) {
    if(++(pos) == cbuf->max)
        pos = 0;
    else
        pos++;
}
*data = cbuf->buffer[pos];
return 0;
}
```


```go
type C[K comparable, V any] cache.Cache[K, V]

type c[K comparable, V any] struct{}

// AsMap implements C.
func (*c[K, V]) AsMap() map[K]V {
	panic("unimplemented")
}

// CleanUp implements C.
func (*c[K, V]) CleanUp() {
	panic("unimplemented")
}

// Get implements C.
func (*c[K, V]) Get(key K) (V, error) {
	panic("unimplemented")
}

// GetAllPresent implements C.
func (*c[K, V]) GetAllPresent(keys []K) (map[K]V, error) {
	panic("unimplemented")
}

// GetAndLoad implements C.
func (*c[K, V]) GetAndLoad(key K, loader func() (V, error)) (V, error) {
	panic("unimplemented")
}

// GetIfPresent implements C.
func (*c[K, V]) GetIfPresent(key K) V {
	panic("unimplemented")
}

// Invalidate implements C.
func (*c[K, V]) Invalidate(key K) {
	panic("unimplemented")
}

// InvalidateAll implements C.
func (*c[K, V]) InvalidateAll() {
	panic("unimplemented")
}

// Put implements C.
func (*c[K, V]) Put(key K, value V) {
	panic("unimplemented")
}

// PutAll implements C.
func (*c[K, V]) PutAll(m map[K]V) {
	panic("unimplemented")
}

// Refresh implements C.
func (*c[K, V]) Refresh(key K) {
	panic("unimplemented")
}

// Size implements C.
func (*c[K, V]) Size() int64 {
	panic("unimplemented")
}

func NewC[K comparable, V any]() C[K, V] {
	myc := c[K, V]{}
	return &myc
}
```
