package cache

import (
	"context"
	"fmt"
	"reflect"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

type foo struct {
	name string
	date time.Time
	dur  time.Duration
	i0   int
}

func Test_Segmentation(t *testing.T) {
	type foo struct {
		i int
		s string
	}
	var newObj = func(i int, s string) foo {
		return foo{i: i, s: s}
	}

	tests := []struct {
		name    string
		cap     int
		inserts []string
		get     []string
		exProt  []string
		exProb  []string
		loader  func(s string) (foo, error)
	}{
		{name: "empty", cap: 3, exProt: []string{}, exProb: []string{}},
		{name: "2 inserts", cap: 3, inserts: []string{"a", "b"}, exProt: []string{}, exProb: []string{"b", "a"}},
		{name: "2 inserts, 2 gets", cap: 3, inserts: []string{"a", "b"}, get: []string{"a", "b"}, exProt: []string{"b", "a"}, exProb: []string{}},
		{
			name:    "14 inserts, 12 gets",
			cap:     10,
			inserts: []string{"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n"},
			get:     []string{"c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n"},
			exProt:  []string{"n", "m", "l", "k", "j", "i", "h", "g"},
			exProb:  []string{"f", "e"},
		},
		{
			name:    "14 inserts, 0 gets",
			cap:     10,
			inserts: []string{"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n"},
			get:     []string{},
			exProt:  []string{},
			exProb:  []string{"n", "m", "l", "k", "j", "i", "h", "g", "f", "e"},
		},
		{
			name:    "14 inserts, 14 reversed gets",
			cap:     10,
			inserts: []string{"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n"},
			get:     []string{"n", "m", "l", "k", "j", "i", "h", "g", "f", "e", "d", "c", "b", "a"},
			exProt:  []string{"e", "f", "g", "h", "i", "j", "k", "l"},
			exProb:  []string{"m", "n"},
		},
		{
			name:    "14 inserts, 14 reverse gets, with loader",
			cap:     10,
			inserts: []string{"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n"},
			get:     []string{"n", "m", "l", "k", "j", "i", "h", "g", "f", "e", "d", "c", "b", "a"},
			exProt:  []string{},
			exProb:  []string{"a", "e", "f", "g", "h", "i", "j", "k", "l", "b"},
			loader: func(s string) (foo, error) {
				return foo{i: 0, s: fmt.Sprint(s, s, s)}, nil
			},
		},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			config := injectDefaults[string, foo](CacheConfig[string, foo]{
				InitialCapacity: test.cap,
				MaximumCapacity: test.cap,
				Loader:          test.loader,
			})
			tc := initFooCache(config)

			for i, key := range test.inserts {
				tc.Put(key, newObj(i, fmt.Sprintf("%s-value", key)))
			}
			for _, key := range test.get {
				tc.Get(key)
			}

			protKeys := tc.protected.Keys()
			assert.Equal(t, test.exProt, protKeys, "protected segment doesn't match")
			probKeys := tc.probation.Keys()
			assert.Equal(t, test.exProb, probKeys, "probation segment doesn't match")

		})
	}

}

func Test_String(t *testing.T) {
	tc := New(CacheConfig[string, foo]{
		InitialCapacity: 10,
		MaximumCapacity: 10,
	}, context.Background())

	//TODO assert this just doens't fail at all

	tc.Put("a", foo{name: "A"})
	tc.Put("b", foo{name: "B"})
	tc.Get("b")
	tc.Get("a")
	tc.Put("c", foo{name: "B"})
	tc.Put("d", foo{name: "B"})
	tc.Put("e", foo{name: "B"})
	tc.Put("f", foo{name: "B"})
	tc.Put("g", foo{name: "B"})
	tc.Put("h", foo{name: "B"})
	tc.Get("f")
	tc.Put("i", foo{name: "B"})
	tc.Get("a")
	tc.Get("b")
	tc.Put("j", foo{name: "B"})
	tc.Put("k", foo{name: "B"})
	tc.Put("l", foo{name: "B"})
	tc.Put("m", foo{name: "B"})
	tc.Put("n", foo{name: "B"})
	tc.Put("o", foo{name: "B"})
	fmt.Println("Add o:\n", tc)
	get(tc, "c", true)
	get(tc, "o", true)
	get(tc, "m", true)
	get(tc, "n", true)
	get(tc, "l", true)
	get(tc, "k", true)
	get(tc, "j", true)
	get(tc, "i", true)
	get(tc, "h", true)
	get(tc, "j", true)
	get(tc, "m", true)
	fmt.Println("Bunch-o-Gets:\n", tc)

	//TODO make a useful Test
}

func get[K comparable, V any](tc Cache[K, V], key K, print bool) {
	_, err := tc.Get(key)
	if print {
		fmt.Println(key, "gotten:\n", tc, "found:", err)
	}
}

func Test_AsMap(t *testing.T) {
	tests := []struct {
		name  string
		input map[string]foo
	}{
		//test fails if the input is nil
		{name: "two elements", input: map[string]foo{"a": {name: "A", i0: 3, dur: 2 * time.Minute}, "b": {name: "B", date: time.Now()}}},
		{name: "empty map", input: map[string]foo{}},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {

			tc := New(CacheConfig[string, foo]{
				InitialCapacity: 10,
				MaximumCapacity: 10,
			}, context.Background())

			getKey := ""
			for k, v := range test.input {
				tc.Put(k, v)
				getKey = k
			}

			if getKey != "" {
				tc.Get(getKey)
				tc.Get(getKey)
				tc.Get(getKey)
				tc.Get(getKey)
				tc.Get(getKey)
			}

			tm := tc.AsMap()
			if !reflect.DeepEqual(test.input, tm) {
				t.Fail()
			}

		})
	}
}
