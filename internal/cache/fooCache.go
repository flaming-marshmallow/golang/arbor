package cache

import (
	"context"
	"fmt"
	"log/slog"
	"reflect"
	"strings"
	"time"

	"gitlab.com/flaming-marshmallow/golang/arbor/internal/list"
)

type fooCache[K comparable, V any] struct {
	initialCapacity   int
	maxCapacity       int
	protectedCapacity int

	expireAfterAccess time.Duration
	expireAfterWrite  time.Duration
	loader            func(K) (V, error)

	cache     map[K]*cacheValue[K, V]
	protected *list.LinkedHashMap[K, bool]
	probation *list.LinkedHashMap[K, bool]

	ticker         time.Ticker
	reaperInterval time.Duration
}

func (fc *fooCache[K, V]) String() string {
	sb := strings.Builder{}
	sb.WriteString("fooCache:\n")
	sb.WriteString(fmt.Sprintf("\t    cache: (len=%d) [", len(fc.cache)))
	delimitter := ""
	for key, _ := range fc.cache {
		sb.WriteString(fmt.Sprintf("%s%v", delimitter, key))
		delimitter = ","
	}
	sb.WriteString("]\n")
	sb.WriteString(fmt.Sprintf("\tprotected: [%+v]\n", fc.protected))
	sb.WriteString(fmt.Sprintf("\tprobation: [%+v]\n", fc.probation))
	return sb.String()
}

func initFooCache[K comparable, V any](config CacheConfig[K, V]) fooCache[K, V] {
	cache := fooCache[K, V]{
		initialCapacity:   config.InitialCapacity,
		maxCapacity:       config.MaximumCapacity,
		protectedCapacity: getCap(config.InitialCapacity, config.SegmentRatio),
		expireAfterAccess: config.ExpireAfterAccess,
		expireAfterWrite:  config.ExpireAfterWrite,
		loader:            config.Loader,
	}
	initData(&cache)
	return cache
}

func initData[K comparable, V any](cache *fooCache[K, V]) {
	cache.cache = make(map[K]*cacheValue[K, V], cache.initialCapacity)
	cache.protected = list.NewLinkedHashMap[K, bool](cache.protectedCapacity)
	cache.probation = list.NewLinkedHashMap[K, bool](cache.initialCapacity)
}

func getCap(max int, segmentRatio float64) int {
	return int(float64(max) * segmentRatio)
}

// runReaper implements selfCleaning.  When this runs, it removes expired cache entries
func (fc *fooCache[K, V]) runReaper(ctx context.Context) {
	//do not run the reaper if the interval isn't set
	if fc.reaperInterval == time.Duration(0) {
		return
	}

	fc.ticker = *time.NewTicker(fc.reaperInterval)
	go func() {
		defer func() {
			if r := recover(); r != nil {
				slog.Warn("there was an error in the reaper")
			}
		}()

		for range fc.ticker.C {
			select {
			case <-ctx.Done():
				return
			default:
			}

			//remove old entries
			fc.removeExpiredEntries()
			//cleanup any overflow if it's present
			fc.cleanupOverflow()
		}
	}()

}

func (fc *fooCache[K, V]) removeExpiredEntries() {

}

// AsMap implements Cache.
func (fc *fooCache[K, V]) AsMap() map[K]V {
	m := make(map[K]V, len(fc.cache))
	for k, cv := range fc.cache {
		m[k] = cv.value
	}
	return m
}

// CleanUp implements Cache.
func (*fooCache[K, V]) CleanUp() {
	panic("unimplemented")
}

// Get implements Cache.
func (fc *fooCache[K, V]) Get(key K) (V, error) {
	return fc.GetAndLoad(key, func() (V, error) {
		return fc.loader(key)
	})
}

// GetAllPresent implements Cache.
func (fc *fooCache[K, V]) GetAllPresent(keys []K) (map[K]V, error) {
	now := time.Now()
	m := make(map[K]V, len(keys))
	for _, key := range keys {
		val, found := fc.cache[key]
		if !found {
			continue
		}
		fc.accessed(now, val)
		m[key] = val.value
	}
	return m, nil
}

// GetAndLoad implements Cache.
func (fc *fooCache[K, V]) GetAndLoad(key K, loader func() (V, error)) (V, error) {
	val, found := fc.cache[key]
	if !found {
		loadedValue, err := loader()
		if err != nil {
			return *new(V), err
		}
		fc.Put(key, loadedValue)
		return loadedValue, nil
	}
	//TODO if we found the key, then we promote to protected if necessary
	now := time.Now()
	fc.accessed(now, val)
	return val.value, nil
}

func (fc *fooCache[K, V]) GetIfPresent(key K) V {
	now := time.Now()
	cVal, found := fc.cache[key]
	if !found {
		return *new(V)
	}

	fc.accessed(now, cVal)
	return cVal.value
}

func (fc *fooCache[K, V]) accessed(now time.Time, cv *cacheValue[K, V]) {
	cv.accessed = now
	protVal, _ := fc.protected.Get(cv.key)
	if protVal {
		return
	}

	v, _ := fc.probation.Get(cv.key)
	if v {
		fc.protected.Put(cv.key, true)
		fc.probation.Remove(cv.key)

		if fc.protectedCapacity < fc.protected.Size() {
			k, _ := fc.protected.Peek(-1)
			// fmt.Printf("+++evicting (%v) from protected to probation\n", k)
			fc.probation.Put(k, true)
			fc.protected.Remove(k)
		}

		fc.cleanupOverflow()
	}
}

// Invalidate implements Cache.
func (fc *fooCache[K, V]) Invalidate(key K) {
	fc.remove(key)
}

// InvalidateAll implements Cache.
func (fc *fooCache[K, V]) InvalidateAll() {
	initData[K, V](fc)
}

func (fc *fooCache[K, V]) Put(key K, value V) {
	now := time.Now()

	cVal, found := fc.cache[key]
	if found {
		//update the access timestamp, but do not promote from probation to protected
		if reflect.DeepEqual(cVal.value, value) {
			cVal.accessed = now
			return
		}
		fc.remove(key)
	}

	newCacheValue := &cacheValue[K, V]{
		key:      key,
		value:    value,
		created:  now,
		written:  now,
		accessed: now,
	}
	fc.putValueInCache(newCacheValue)
}

// PutAll implements Cache.
func (fc *fooCache[K, V]) PutAll(m map[K]V) {
	for k, v := range m {
		fc.Put(k, v)
	}
}

// Refresh implements Cache.
func (fc *fooCache[K, V]) Refresh(key K) {
	value, err := fc.loader(key)
	if err != nil {
		//TODO need to deal with this
		slog.Error("error loading cache value", "key", key, err)
	}
	fc.Put(key, value)
}

// Size implements Cache.
func (fc *fooCache[K, V]) Size() int64 {
	return int64(len(fc.cache))
}

func (fc *fooCache[K, V]) overstuffed() bool {
	return fc.maxCapacity < (fc.probation.Size() + fc.protected.Size())
	// return fc.maxCapacity < fc.cache.Size()
}

func (fc *fooCache[K, V]) putValueInCache(value *cacheValue[K, V]) (evicted *cacheValue[K, V]) {
	//if evicted, then we've overwritten a key
	evicted, _ = fc.remove(value.key)
	// if evicted != nil {
	// 	fmt.Printf("put=%v, evicted=%v\n", value.key, evicted.key)
	// }

	fc.cache[value.key] = value
	fc.probation.Put(value.key, true)
	fc.cleanupOverflow()
	return
}

// func (fc *fooCache[K, V]) cleanupProtectedOverflow() {
// 	if fc.protectedCapacity < fc.protected.Size() {
// 		protKey, _ := fc.protected.Peek(-1)
// 		fc.protected.Remove(protKey)
// 		fc.probation.Put(protKey, true)
// 	}
// }

// Evicts the least recently accessed key.  If the oldest value is in the protected segment, it
// is evicted to the probation segment and the oldest value in the probation
// segment is evicted from the cache.
func (fc *fooCache[K, V]) cleanupOverflow() {
	for fc.overstuffed() {
		// fmt.Println("+++ Cache is Overfull +++\n", fc)
		protKey, _ := fc.protected.Peek(-1)
		probKey, _ := fc.probation.Peek(-1)

		probValue, _ := fc.cache[probKey]
		protValue, _ := fc.cache[protKey]

		if protValue != nil && protValue.accessed.Before(probValue.accessed) {
			//TODO expire protected
			fc.protected.Remove(protKey)
			fc.probation.Put(protKey, true)
		} else {
			//expire probation
			delete(fc.cache, probKey)
			fc.probation.Remove(probKey)
			// fmt.Printf("***evicted due to overfilling: %v\n", probKey)
		}
		// fmt.Println("end of loop:\n\t", fc)
	}
}

func (fc *fooCache[K, V]) remove(key K) (*cacheValue[K, V], error) {
	evicted, found := fc.cache[key]
	if !found {
		return nil, ErrNotFound
	}
	delete(fc.cache, key)
	fc.protected.Remove(key)
	fc.probation.Remove(key)
	return evicted, nil
}
