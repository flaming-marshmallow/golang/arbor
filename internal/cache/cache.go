package cache

import (
	"context"
	"fmt"
	"log/slog"
	"time"

	"gitlab.com/flaming-marshmallow/golang/arbor/internal/list"
)

// var _ Cache[string, any] = (*segmentedLru[string, any])(nil)

var _ Cache[string, any] = (*fooCache[string, any])(nil)
var _ selfCleaning = (*fooCache[string, any])(nil)

var ErrNotFound error = list.ErrNotFound

type Cache[K comparable, V any] interface {
	//Returns a thread-safe map view of the entries.
	AsMap() map[K]V
	//Performs any pending maintenance operations
	CleanUp()
	Get(key K) (V, error)
	GetAndLoad(key K, loader func() (V, error)) (V, error)
	GetAllPresent(keys []K) (map[K]V, error)
	GetIfPresent(key K) V
	Invalidate(key K)
	InvalidateAll()
	Put(key K, value V)
	PutAll(m map[K]V)
	//Loads a new value of the key.  possibly asynchronously.
	Refresh(key K)
	Size() int64
	// Stats() CacheStats
}

type selfCleaning interface {
	runReaper(ctx context.Context)
}

type CacheConfig[K comparable, V any] struct {
	InitialCapacity int
	MaximumCapacity int

	Loader        func(K) (V, error)
	StatsRecorder StatsRecorder

	EvictionHandler func(key K, value V)

	//Duration after access(read) to ensure a record is not expired.
	ExpireAfterAccess time.Duration

	//Duration after write to ensure a record is not expired.
	ExpireAfterWrite time.Duration

	//refresh the access timestamp after writing a new value to a key
	RefreshAfterWrite time.Duration

	// Logger any //TODO

	SegmentRatio float64

	//this stuff is from guava, but I'm not sure we really need it
	MaximumWeight int64
	SoftValues    bool
	WeakKeys      bool
	WeakValues    bool
	Weigher       func() error
}

var minTimeToExpire = 2 * time.Minute

var defaultSegmentRatio = 0.80

func defaultLoader[K comparable, V any]() func(K) (V, error) {
	return func(key K) (V, error) {
		slog.Debug("default loader called", "key", key)
		return *new(V), fmt.Errorf("no loader defined")
	}
}

func defaultEvictionHandler[K comparable, V any]() func(K, V) {
	return func(key K, value V) {
		slog.Debug("eviction handler called", "key", key, "value", value)
	}
}

func New[K comparable, V any](config CacheConfig[K, V], ctx context.Context) Cache[K, V] {

	config = injectDefaults[K, V](config)

	//TODO validateConfig(config)

	newCache := initFooCache[K, V](config)

	//TODO add a ticker to run this every now and then...
	//newCache.(selfcleaning).runReaper(ctx);
	var sc fooCache[K, V] = newCache
	sc.runReaper(ctx)

	return &newCache
}

func injectDefaults[K comparable, V any](config CacheConfig[K, V]) CacheConfig[K, V] {
	loader := config.Loader
	if loader == nil {
		config.Loader = defaultLoader[K, V]()
	}

	statsRecorder := config.StatsRecorder
	if statsRecorder == nil {
		config.StatsRecorder = &defaultStatsRecorder{}
	}

	evictionHandler := config.EvictionHandler
	if evictionHandler == nil {
		config.EvictionHandler = defaultEvictionHandler[K, V]()
	}

	expireAfterAccess := config.ExpireAfterAccess
	if expireAfterAccess < minTimeToExpire {
		config.ExpireAfterAccess = minTimeToExpire
	}

	segmentRatio := config.SegmentRatio
	if segmentRatio == 0 {
		config.SegmentRatio = defaultSegmentRatio
	}
	return config
}

func StopCache(ctx context.Context) {
	//TODO
}

type cacheValue[K comparable, V any] struct {
	key      K
	value    V
	created  time.Time
	accessed time.Time
	written  time.Time
}
