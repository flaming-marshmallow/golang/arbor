package cache

import "time"

var _ StatsRecorder = (*defaultStatsRecorder)(nil)
var _ StatsRecorder = (*openTelemetryStatsRecorder)(nil)

type StatsRecorder interface {
	AddHit()
	AddMiss()
	AddLoadAttempt()
	AddLoadError()
	AddLoadSuccess()
	AddLoadTime(time.Duration)
	AddEviction()
}

// TODO opentelemetry we want the context, so lets include it in our statsrecorder interface
type openTelemetryStatsRecorder struct{}

// AddEviction implements StatsRecorder.
func (*openTelemetryStatsRecorder) AddEviction() {
	panic("unimplemented")
}

// AddHit implements StatsRecorder.
func (*openTelemetryStatsRecorder) AddHit() {
	panic("unimplemented")
}

// AddLoadAttempt implements StatsRecorder.
func (*openTelemetryStatsRecorder) AddLoadAttempt() {
	panic("unimplemented")
}

// AddLoadError implements StatsRecorder.
func (*openTelemetryStatsRecorder) AddLoadError() {
	panic("unimplemented")
}

// AddLoadSuccess implements StatsRecorder.
func (*openTelemetryStatsRecorder) AddLoadSuccess() {
	panic("unimplemented")
}

// AddLoadTime implements StatsRecorder.
func (*openTelemetryStatsRecorder) AddLoadTime(time.Duration) {
	panic("unimplemented")
}

// AddMiss implements StatsRecorder.
func (*openTelemetryStatsRecorder) AddMiss() {
	panic("unimplemented")
}

type defaultStatsRecorder struct {
	hits          int64
	misses        int64
	loadAttemts   int64
	loadErrors    int64
	loadSuccesses int64
	loadTime      time.Duration
	evictions     int64
}

//https://javadoc.io/doc/com.google.guava/guava/latest/com/google/common/cache/CacheStats.html

func (sr defaultStatsRecorder) AverageLoadPenalty() int64 {
	panic("")
}
func (sr defaultStatsRecorder) EvictionCount() int {
	panic("")
}
func (sr defaultStatsRecorder) HitCount() int {
	panic("")
}
func (sr defaultStatsRecorder) HitRate() float32 {
	panic("")
}
func (sr defaultStatsRecorder) LoadCount() int {
	panic("")
}
func (sr defaultStatsRecorder) LoadExceptionCount() int {
	panic("")
}
func (sr defaultStatsRecorder) LoadExceptionRate() float32 {
	panic("")
}
func (sr defaultStatsRecorder) LoadSuccessCount() int {
	panic("")
}
func (sr defaultStatsRecorder) MissCount() int {
	panic("")
}
func (sr defaultStatsRecorder) MissRate() float32 {
	panic("")
}
func (sr defaultStatsRecorder) RequestCount() int {
	panic("")
}
func (sr defaultStatsRecorder) TotalLoadTime() int64 {
	panic("")
}

// AddEviction implements StatsRecorder.
func (sr *defaultStatsRecorder) AddEviction() {
	sr.evictions++
}

// AddHit implements StatsRecorder.
func (sr *defaultStatsRecorder) AddHit() {
	sr.hits++
}

// AddLoadAttempt implements StatsRecorder.
func (sr *defaultStatsRecorder) AddLoadAttempt() {
	sr.loadAttemts++
}

// AddLoadError implements StatsRecorder.
func (sr *defaultStatsRecorder) AddLoadError() {
	sr.loadErrors++
}

// AddLoadSuccess implements StatsRecorder.
func (sr *defaultStatsRecorder) AddLoadSuccess() {
	sr.loadSuccesses++
}

// AddLoadTime implements StatsRecorder.
func (sr *defaultStatsRecorder) AddLoadTime(loadTime time.Duration) {
	sr.loadTime += loadTime
}

// AddMiss implements StatsRecorder.
func (sr *defaultStatsRecorder) AddMiss() {
	sr.misses++
}
