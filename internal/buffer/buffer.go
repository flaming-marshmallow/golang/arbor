package buffer

import (
	"fmt"
	"sync"
)

/*
 * ensure our structs satisfy the Buffer interface
 */

var _ Buffer[any] = (*circularBuffer[any])(nil)
var _ Buffer[any] = (*concurrentCircularBuffer[any])(nil)

type Buffer[T any] interface {
	Push(T)
	//Returns an error if the buffer is full.
	SafePush(T) error
	Pop() T
	Empty() bool
	Full() bool
	Size() int
	Capacity() int
	Reset()
	String() string
	//lookAhead is how far out to look, 0 is the next element, 0+1 is the element after, etc. up to the buffer size.
	Peek(lookAhead int) T
}

var ErrFullBuffer error = fmt.Errorf("buffer full")

type BufferConfig struct {
	concurrent bool
	max        int
}

func New[T any](config BufferConfig) Buffer[T] {

	buffer := circularBuffer[T]{
		buffer: make([]*element[T], config.max),
		head:   0,
		tail:   0,
		max:    config.max,
		full:   false,
	}
	if config.concurrent {
		return &concurrentCircularBuffer[T]{
			buffer: buffer,
			mu:     sync.Mutex{},
		}
	}
	return &buffer
}

type element[T any] struct {
	value T
}
