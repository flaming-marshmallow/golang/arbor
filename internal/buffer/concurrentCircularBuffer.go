package buffer

import (
	"sync"
)

//TODO push into channel, then have a go routine injecting into the buffer
// Overloads the functions we need mutexes in.

type concurrentCircularBuffer[T any] struct {
	buffer circularBuffer[T]
	mu     sync.Mutex
}

// Pop implements Buffer.
func (ccb *concurrentCircularBuffer[T]) Pop() T {
	ccb.mu.Lock()
	defer ccb.mu.Unlock()

	return ccb.buffer.Pop()
}

func (ccb *concurrentCircularBuffer[T]) SafePush(t T) error {
	if ccb.buffer.Full() {
		return ErrFullBuffer
	}
	ccb.mu.Lock()
	defer ccb.mu.Unlock()

	if ccb.buffer.Full() {
		return ErrFullBuffer
	}

	ccb.buffer.Push(t)
	return nil
}

func (ccb *concurrentCircularBuffer[T]) Peek(lookAhead int) T {
	ccb.mu.Lock()
	defer ccb.mu.Unlock()
	return ccb.buffer.Peek(lookAhead)
}

// Push implements Buffer.
func (ccb *concurrentCircularBuffer[T]) Push(t T) {
	ccb.mu.Lock()
	defer ccb.mu.Unlock()

	ccb.buffer.Push(t)
}

// Reset implements Buffer.
func (ccb *concurrentCircularBuffer[T]) Reset() {
	ccb.mu.Lock()
	defer ccb.mu.Unlock()
	ccb.buffer.Reset()
}

func (ccb *concurrentCircularBuffer[T]) Capacity() int {
	return ccb.buffer.Capacity()
}

func (ccb *concurrentCircularBuffer[T]) Size() int {
	ccb.mu.Lock()
	defer ccb.mu.Unlock()
	return ccb.buffer.Size()
}

func (ccb *concurrentCircularBuffer[T]) Empty() bool {
	ccb.mu.Lock()
	defer ccb.mu.Unlock()
	return ccb.buffer.Empty()
}

func (ccb *concurrentCircularBuffer[T]) Full() bool {
	ccb.mu.Lock()
	defer ccb.mu.Unlock()
	return ccb.buffer.Full()
}

func (ccb *concurrentCircularBuffer[T]) String() string {
	ccb.mu.Lock()
	defer ccb.mu.Unlock()
	return ccb.buffer.String()
}
