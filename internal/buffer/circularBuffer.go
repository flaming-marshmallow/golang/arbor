package buffer

import (
	"fmt"
)

// https://embeddedartistry.com/blog/2017/05/17/creating-a-circular-buffer-in-c-and-c/

// write at head
// read at tail

type circularBuffer[T any] struct {
	buffer []*element[T]
	head   int
	tail   int
	max    int
	full   bool
}

// Used to advance the head pointer during a write operation.
// If the buffer is full, then the tail also needs to be advanced forward.
func (ecb *circularBuffer[T]) advancePointer() {
	if ecb.full {
		ecb.tail++
		if ecb.tail == ecb.max {
			ecb.tail = 0
		}
	}
	ecb.head++
	if ecb.head == ecb.max {
		ecb.head = 0
	}
	ecb.full = ecb.head == ecb.tail
}

func (ecb *circularBuffer[T]) retreatPointer() {
	ecb.full = false
	ecb.tail++
	if ecb.tail == ecb.max {
		ecb.tail = 0
	}
}

func (ecb *circularBuffer[T]) Peek(lookAhead int) T {
	if ecb.Empty() || ecb.max < lookAhead {
		return *new(T)
	}
	pos := ecb.tail
	for i := 0; i < lookAhead; i++ {
		if pos == ecb.max {
			pos = 0
		} else {
			pos++
		}
	}
	element := ecb.buffer[pos]
	if element == nil {
		return *new(T)
	}
	return element.value
}

func (ecb *circularBuffer[T]) String() string {
	result := "["
	delimitter := ""
	for i, val := range ecb.buffer {
		result += fmt.Sprintf("%s(%d: %v)", delimitter, i, val)
		delimitter = ", "
	}
	result += "]\n"
	result += fmt.Sprintf("\tread: %d, write: %d", ecb.tail, ecb.head)
	return result
}

// Capacity implements Buffer.
func (ecb *circularBuffer[T]) Capacity() int {
	return ecb.max
}

// Empty implements Buffer.
func (ecb *circularBuffer[T]) Empty() bool {
	return !ecb.full && (ecb.head == ecb.tail)
}

// Full implements Buffer.
func (ecb *circularBuffer[T]) Full() bool {
	return ecb.full
}

// Pop implements Buffer.
func (ecb *circularBuffer[T]) Pop() T {
	if ecb.Empty() {
		return *new(T)
	}

	element := ecb.buffer[ecb.tail]
	ecb.retreatPointer()
	return element.value
}

func (ecb *circularBuffer[T]) SafePush(t T) error {
	if ecb.Full() {
		return ErrFullBuffer
	}
	ecb.Push(t)
	return nil
}

// Push implements Buffer.
func (ecb *circularBuffer[T]) Push(t T) {
	ecb.buffer[ecb.head] = &element[T]{value: t}
	ecb.advancePointer()
}

// Reset implements Buffer.
func (ecb *circularBuffer[T]) Reset() {
	ecb.head = ecb.tail
	ecb.full = false
}

// Size implements Buffer.
func (ecb *circularBuffer[T]) Size() int {
	size := ecb.max
	if !ecb.full {
		if ecb.head >= ecb.tail {
			size = ecb.head - ecb.tail
		} else {
			size = ecb.max + ecb.head - ecb.tail
		}
	}
	return size
}
