package buffer

import (
	"fmt"
	"reflect"
	"testing"

	"github.com/stretchr/testify/assert"
)

type TestElement struct {
	val string
}

var testElements = []TestElement{
	{val: "ZERO"},
	{val: "one"},
	{val: "two"},
	{val: "3"},
	{val: "4"},
	{val: "5"},
	{val: "6"},
	{val: "7"},
	{val: "8"},
	{val: "9"},
	{val: "ten"},
	{val: "11"},
	{val: "13"},
	{val: "14"},
	{val: "15"},
	{val: "16"},
	{val: "17"},
	{val: "18"},
	{val: "19"},
	{val: "20"},
	{val: "21"},
}

var records []string = []string{}

func init() {
	fmt.Println("setting up test data")
	for i := 0; i < 10000; i++ {
		records = append(records, fmt.Sprintf("rec: %d", i))
	}
}

/*
 * Benchmarks
 */

func Benchmark_Buffer_Push(b *testing.B) {
	buffer := New[string](BufferConfig{max: 100})

	for i := 0; i < 1000; i++ {
		for j := 0; j < 50; j++ {
			buffer.Push(records[j])
		}
		for j := 0; j < 8; j++ {
			buffer.Pop()
		}
	}
}

func Benchmark_ConcurrentBuffer_Push(b *testing.B) {
	buffer := New[string](BufferConfig{max: 100, concurrent: true})

	for i := 0; i < 1000; i++ {
		for j := 0; j < 50; j++ {
			buffer.Push(records[j])
		}
		for j := 0; j < 8; j++ {
			buffer.Pop()
		}
	}

}

/*
 * Tests
 */

// func Test_SafePush_doubleCheck(t *testing.T) {
// 	buffer := concurrentCircularBuffer[string]{
// 		circularBuffer: circularBuffer[string]{
// 			buffer: make([]*Element[string], 3),
// 			head:   0,
// 			tail:   0,
// 			max:    3,
// 			full:   false,
// 		},
// 		mu: sync.Mutex{},
// 	}

// 	buffer.Push("abc")
// 	buffer.Push("def")
// 	assert.False(t, buffer.Full())

// 	buffer.mu.Lock()

// 	var err error
// 	go func() {
// 		err = buffer.SafePush("TTT")
// 	}()

// 	buffer.Push("ghi")
// 	buffer.Push("jkl")
// 	assert.True(t, buffer.Full())

// 	buffer.mu.Unlock()
// 	assert.ErrorIs(t, ErrFullBuffer, err)

// }

func Test_Buffers(t *testing.T) {
	tests := []struct {
		name   string
		config BufferConfig
		values []TestElement
	}{
		// {name: "ring buffer", buffer: TestBuffer{buffer: func(cap int) Buffer[TestElement] { return NewRingBuffer[TestElement](cap) }, capacity: 10}, values: testElements},
		{name: "circular buffer", config: BufferConfig{max: 10}, values: testElements},
		{name: "concurrent circular buffer", config: BufferConfig{max: 10, concurrent: true}, values: testElements},
	}

	for _, test := range tests {
		testBuffers[TestElement](t, test.name, test.config, test.values)
		testReset(t, test.name, test.config)
		testSafePush(t, test.name, test.config)
		testCapacity(t, test.name, test.config)
	}
}

func testCapacity(t *testing.T, name string, config BufferConfig) {
	t.Run(name+"_capacity", func(t *testing.T) {
		buffer := New[TestElement](config)
		assert.Equal(t, config.max, buffer.Capacity())
	})
}

func testSafePush(t *testing.T, name string, config BufferConfig) {
	t.Run(name+"_safe-push", func(t *testing.T) {
		buffer := New[TestElement](config)
		for i := 0; i < config.max; i++ {
			err := buffer.SafePush(TestElement{val: "abc"})
			assert.NoError(t, err)
		}
		err := buffer.SafePush(TestElement{val: "abc"})
		// assert.Error(t, err)
		assert.ErrorIs(t, err, ErrFullBuffer)
	})
}

func testReset(t *testing.T, name string, config BufferConfig) {
	t.Run(name+"_reset", func(t *testing.T) {
		buffer := New[TestElement](config)
		buffer.Push(TestElement{})
		buffer.Push(TestElement{})
		buffer.Push(TestElement{})
		buffer.Push(TestElement{})
		buffer.Push(TestElement{})
		assert.Equal(t, 5, buffer.Size())
		buffer.Reset()
		assert.True(t, buffer.Empty())
		assert.False(t, buffer.Full())
		assert.Equal(t, 0, buffer.Size())
	})
}

func testBuffers[T any](t *testing.T, name string, config BufferConfig, values []TestElement) {
	tests := []struct {
		name              string
		config            BufferConfig
		capacity          int
		addCount          int
		popCount          int
		expectEndSize     int
		expectedPopValues int
	}{
		{name: name + ": 0in 0out", config: config},
		{name: name + ": one in none out", config: config, addCount: 1, popCount: 0, expectEndSize: 1},
		{name: name + ": 2 in none out", config: config, addCount: 2, popCount: 0, expectEndSize: 2},
		{name: name + ": 9 in none out", config: config, addCount: 9, popCount: 0, expectEndSize: 9},
		{name: name + ": 10 in none out", config: config, addCount: 10, popCount: 0, expectEndSize: 10},
		{name: name + ": 11 in none out", config: config, addCount: 11, popCount: 0, expectEndSize: 10},

		{name: name + ": one in one out", config: config, addCount: 1, popCount: 1, expectEndSize: 0, expectedPopValues: 1},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			buffer := New[TestElement](test.config)

			//our buffer starts empty
			assert.True(t, buffer.Empty())
			assert.False(t, buffer.Full())
			assert.Equal(t, 0, buffer.Size())

			//push onto the buffer
			for i := 0; i < test.addCount; i++ {
				buffer.Push(values[i])
			}
			//pop from the buffer
			popped := 0
			for i := 0; i < test.popCount; i++ {
				val := buffer.Pop()
				if !reflect.DeepEqual(val, TestElement{}) {
					popped++
				}
			}
			assert.Equal(t, test.expectedPopValues, popped, "popped count is incorrect")
			size := buffer.Size()
			assert.Equal(t, test.expectEndSize, size, "end size is incorrect")
		})
	}

}

func Test_Size(t *testing.T) {
	tests := []struct {
		name          string
		bufferSize    int
		opOrder       []string
		expectedSize  int
		expectedEmpty bool
		expectedFull  bool
	}{
		{name: "pop 3 times only", expectedSize: 0, bufferSize: 4, expectedEmpty: true, opOrder: []string{"pop", "pop", "pop"}},
		{name: "zero", expectedSize: 0, bufferSize: 4, expectedEmpty: true},
		{name: "one", expectedSize: 1, bufferSize: 4, opOrder: []string{"push"}},
		{name: "full", expectedSize: 4, bufferSize: 4, expectedFull: true, opOrder: []string{"push", "push", "push", "push"}},
		{name: "full plus one", expectedSize: 4, bufferSize: 4, expectedFull: true, opOrder: []string{"push", "push", "push", "push", "push"}},
		{
			name: "full plus one minus one", expectedSize: 3, bufferSize: 4,
			opOrder: []string{"push", "push", "push", "push", "push", "pop"},
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			buffer := circularBuffer[TestElement]{
				buffer: make([]*element[TestElement], test.bufferSize),
				max:    test.bufferSize,
			}
			for i, op := range test.opOrder {
				switch op {
				case "push":
					buffer.Push(testElements[i])
				case "pop":
					buffer.Pop()
				}
			}

			assert.Equal(t, test.expectedSize, buffer.Size())
			assert.Equal(t, test.expectedEmpty, buffer.Empty(), "empty is wrong")
			assert.Equal(t, test.expectedFull, buffer.Full(), "full is wrong")
		})
	}

}

func Test_Foo(t *testing.T) {

	foo := []string{"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23"}

	buffer := New[string](BufferConfig{max: 10, concurrent: false})
	buffer.Push(foo[0])
	buffer.Push(foo[1])
	buffer.Push(foo[2])

	assert.Equal(t, 3, buffer.Size())

	buffer.Pop()
	assert.Equal(t, 2, buffer.Size())
	buffer.Pop()
	buffer.Pop()
	buffer.Pop()
	buffer.Pop()
	buffer.Pop()
	buffer.Pop()
	assert.Equal(t, 0, buffer.Size())
	buffer.Push(foo[3])
	assert.Equal(t, 1, buffer.Size())
	buffer.Pop()

	for _, v := range foo[4:23] {
		buffer.Push(v)
	}
	assert.Equal(t, 10, buffer.Size())

	v := buffer.Pop()
	assert.Equal(t, foo[13], v)

}
