package server

import (
	"crypto/md5"
	"encoding/hex"
	"log/slog"
	"strconv"
)

/*

consistency level
0: one node is good enough
1: majority of partitions have copy
2: all partitions have copy


N partitions

if 1 server, partitions don't matter

*/

type ShardFunc func(key string, p int) int

func mySharder(key string, p int, maxPartitions int) int {

	firstN := FirstN(key, p)

	hash := md5.Sum([]byte(firstN))
	c := hex.EncodeToString(hash[:])
	i, _ := strconv.ParseInt(c, 16, 64)

	result := i % int64(maxPartitions)
	slog.Debug("shard", "key", key, "firstN", firstN, "hash", c, "int", i, "result", result)
	return int(result)
}

type KeyStore[K comparable, V any] interface {
	Put(k K, v V) (V, error)
	Get(k K) (V, error)
}

// Returns the fist N characters of a string.
func FirstN(s string, n int) string {
	i := 0
	for j := range s {
		if i == n {
			return s[:j]
		}
		i++
	}
	return s
}

// func newKeyStore() KeyStore[string, any] {
// 	return &keyValueStore[string, any]{}
// }

// type keyValueStore[K comparable, V any] struct {
// 	store map[K]V
// }

// // Get implements KeyStore.
// func (kvs *keyValueStore[K, V]) Get(k K) (V, error) {
// 	return kvs.store[k], nil
// }

// // Put implements KeyStore.
// func (kvs *keyValueStore[K, V]) Put(k K, v V) (V, error) {
// 	oldValue := kvs.store[k]
// 	kvs.store[k] = v
// 	return oldValue, nil
// }
