package server

import "context"

type Server interface {
	Start(ctx context.Context) error
	Stop(ctx context.Context) error
	ForceStop(ctx context.Context) error
}

// func NewServer() Server {
// 	return &server{
// 		store: newKeyStore(),
// 	}
// }

// type server struct {
// 	store KeyStore[string, any]
// }

// // ForceStop implements Server.
// func (*server) ForceStop(ctx context.Context) error {
// 	panic("unimplemented")
// }

// // Start implements Server.
// func (*server) Start(ctx context.Context) error {
// 	panic("unimplemented")
// }

// // Stop implements Server.
// func (*server) Stop(ctx context.Context) error {
// 	panic("unimplemented")
// }
