package server

import (
	"log/slog"
	"os"
	"testing"
)

func init() {
	// initialize the debug logger for tests
	logger := slog.New(slog.NewTextHandler(os.Stdout, &slog.HandlerOptions{Level: slog.LevelDebug, AddSource: true}))
	slog.SetDefault(logger)
}

func Test_mySharder(t *testing.T) {
	type args struct {
		key string
		p   int
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{name: "abc", args: args{key: "abc", p: 2}, want: 4},
		{name: "asfdsadfasdfa", args: args{key: "asfdsadfasdfa dfdsf adf26t524 346t", p: 2}, want: 5},
		{name: "a", args: args{key: "a", p: 2}, want: 2},
		{name: "b", args: args{key: "b", p: 2}, want: 6},
		{name: "ddddddddddddddddddddddddd", args: args{key: "ddddddddddddddddddddddddd", p: 2}, want: 6},
		{name: "dd", args: args{key: "dd", p: 2}, want: 6},
		{name: "dddd123456", args: args{key: "dddd123456", p: 2}, want: 6},
		{name: "dddd123456", args: args{key: "dddd123456", p: 2}, want: 6},
		{name: "dddd123456", args: args{key: "dddd123456", p: 2}, want: 6},
		{name: "dddd123456", args: args{key: "dddd123456", p: 2}, want: 6},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := mySharder(tt.args.key, tt.args.p, 10); got != tt.want {
				// t.Errorf("mySharder() = %v, want %v", got, tt.want)
			}
		})
	}
}
