package list

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_Peek(t *testing.T) {
	type testValue struct {
		i int
		s string
	}
	type test struct {
		peekIdx       int
		expectedKey   string
		expectedValue testValue
	}
	tests := []struct {
		name   string
		values []testValue
		peeks  []test
	}{
		{
			name: "empty",
			peeks: []test{
				{peekIdx: 0, expectedKey: *new(string), expectedValue: *new(testValue)},
				{peekIdx: -1, expectedKey: *new(string), expectedValue: *new(testValue)},
				{peekIdx: 1, expectedKey: *new(string), expectedValue: *new(testValue)}},
		},
		{
			name: "one element",
			values: []testValue{
				{i: 1, s: "1"},
			},
			peeks: []test{
				{peekIdx: 0, expectedKey: "1", expectedValue: testValue{i: 1, s: "1"}},
				{peekIdx: -1, expectedKey: "1", expectedValue: testValue{i: 1, s: "1"}},
				{peekIdx: 1, expectedKey: "1", expectedValue: testValue{i: 1, s: "1"}},
			},
		},
		{
			name: "multi-valued",
			values: []testValue{
				{i: 1, s: "1"},
				{i: 2, s: "2"},
				{i: 3, s: "3"},
				{i: 4, s: "4"},
				{i: 5, s: "5"},
				{i: 6, s: "6"},
				{i: 7, s: "7"},
				{i: 8, s: "8"},
				{i: 9, s: "9"},
				{i: 10, s: "10"},
			},
			peeks: []test{

				{peekIdx: 0, expectedKey: "10", expectedValue: testValue{i: 10, s: "10"}},
				{peekIdx: -1, expectedKey: "1", expectedValue: testValue{i: 1, s: "1"}},
				{peekIdx: -11, expectedKey: "1", expectedValue: testValue{i: 1, s: "1"}},
				{peekIdx: -21, expectedKey: "1", expectedValue: testValue{i: 1, s: "1"}},
				{peekIdx: 1, expectedKey: "9", expectedValue: testValue{i: 9, s: "9"}},
				{peekIdx: 11, expectedKey: "9", expectedValue: testValue{i: 9, s: "9"}},
				{peekIdx: 21, expectedKey: "9", expectedValue: testValue{i: 9, s: "9"}},
				{peekIdx: 20, expectedKey: "10", expectedValue: testValue{i: 10, s: "10"}},
			},
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			l := NewLinkedHashMap[string, testValue](10)
			for _, v := range test.values {
				l.Put(fmt.Sprintf("%d", v.i), v)
			}
			fmt.Println("MAP:", l.String())

			for _, peeks := range test.peeks {
				t.Run(fmt.Sprintf("%s_%v", test.name, peeks.peekIdx), func(t *testing.T) {
					k, v := l.Peek(peeks.peekIdx)
					fmt.Printf("%s: %v->%v\n", test.name, k, v)
					assert.Equal(t, peeks.expectedKey, k, "key peek at %v", peeks.peekIdx)
					assert.Equal(t, peeks.expectedValue, v, "value peek at %v", peeks.peekIdx)
				})
			}
		})
	}
}

func Test_linkedHashmap(t *testing.T) {

	tests := []struct {
		name string
	}{
		{name: "test"},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			lhm := NewLinkedHashMap[string, string](10)
			assert.NotNil(t, lhm.m)
			assert.NotNil(t, lhm.l)

			assert.Equal(t, 0, lhm.Size())
			assert.True(t, lhm.IsEmpty())

			oldValue := lhm.Put("abc", "ABC")

			assert.Equal(t, *new(string), oldValue)
			assert.Equal(t, 1, lhm.Size())
			assert.Equal(t, 1, len(lhm.m))
			assert.Equal(t, 1, lhm.l.Len())

			value, err := lhm.Get("abc")
			assert.Nil(t, err)
			assert.Equal(t, "ABC", value)

			value, err = lhm.Get("def")
			assert.ErrorIs(t, ErrNotFound, err)
			assert.Equal(t, *new(string), value)

			oldValue, err = lhm.Remove("ghi")
			assert.ErrorIs(t, err, ErrNotFound)
			assert.Equal(t, *new(string), oldValue)

			oldValue, err = lhm.Remove("abc")
			assert.Nil(t, err)
			assert.Equal(t, "ABC", oldValue)
			assert.Equal(t, 0, lhm.Size())
			assert.True(t, lhm.IsEmpty())
			assert.Equal(t, 0, len(lhm.m))
			assert.Equal(t, 0, lhm.l.Len())

			//last insert is at the head
			pairs := []struct {
				key   string
				value string
			}{
				{"ttt", "TTT"}, {"ddd", "DDD"}, {"eee", "EEE"},
			}
			for _, pair := range pairs {
				lhm.Put(pair.key, pair.value)
				element := lhm.l.Front()
				assert.Equal(t, pair.value, element.Value.value)
			}

			//get moves the element to the head
			val, err := lhm.Get(pairs[1].key)
			assert.Nil(t, err)
			assert.Equal(t, pairs[1].value, val)
			element := lhm.l.Front()
			assert.Equal(t, pairs[1].value, element.Value.value)
			assert.Equal(t, pairs[1].key, element.Value.key)
			assert.Equal(t, 3, lhm.Size())

			fmt.Printf("KEYS:::: %+v\n", lhm.Keys())

		})
	}

}
