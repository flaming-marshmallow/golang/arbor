package list

import (
	"fmt"
	"strings"
)

//TODO clear and new should call a common init() function

//TODO make the "move to front" indexing optional

var _ HashMap[string, any] = (*LinkedHashMap[string, any])(nil)

// TODO consider returning a few more errors
// TODO should it have gethead/gettail
type HashMap[K comparable, V any] interface {
	AsMap() map[K]V
	Clear()
	Keys() []K
	Len() int
	Size() int
	IsEmpty() bool
	Put(K, V) V
	Get(K) (V, error)
	Remove(K) (V, error)
	RemoveTail() V

	//Peek(0) is the first element, Peek(-1) is the last element
	Peek(int) (K, V)
}

// Error returned if the key is not found in the map.
var ErrNotFound = fmt.Errorf("not found")

// LinkedHashMap internally is a map and a linked list, both
// using pointers to common elements.
type LinkedHashMap[K comparable, V any] struct {
	m               map[K]*Element[mapEntry[K, V]]
	l               List[mapEntry[K, V]]
	initialCapacity int
}

func NewLinkedHashMap[K comparable, V any](capacity int) *LinkedHashMap[K, V] {
	m := LinkedHashMap[K, V]{
		m:               make(map[K]*Element[mapEntry[K, V]], capacity),
		l:               List[mapEntry[K, V]]{},
		initialCapacity: capacity,
	}
	return &m
}

type mapEntry[K comparable, V any] struct {
	key   K
	value V
}

/*
linked hashmap remains in access order
*/

func (l *LinkedHashMap[K, V]) String() string {
	var sb strings.Builder

	delimitter := ""
	entry := l.l.Front()
	for entry != nil {
		sb.WriteString(fmt.Sprintf("%s%v:%v", delimitter, entry.Value.key, entry.Value.value))
		delimitter = ","

		entry = entry.Next()
	}
	return sb.String()
}

func (l *LinkedHashMap[K, V]) Peek(idx int) (K, V) {
	if len(l.m) == 0 {
		return *new(K), *new(V)
	}

	var cur *Element[mapEntry[K, V]]

	if idx == 0 {
		cur = l.l.Front()
	} else if idx < 0 {
		cur = l.l.Back()
		for i := -2; i >= idx; i-- {
			// fmt.Printf("\t\tidx=%d, i=%d\n", idx, i)
			cur = cur.prev
			if cur == &l.l.root {
				cur = cur.prev
			}
			// fmt.Printf("\t\tcur=%+v\n", cur.Value)
		}
	} else if idx > 0 {
		cur = l.l.Front()
		for i := 1; i <= idx; i++ {
			cur = cur.next
			if cur == &l.l.root {
				cur = cur.next
			}
		}
	}
	return cur.Value.key, cur.Value.value
}

func (l *LinkedHashMap[K, V]) AsMap() map[K]V {
	m := make(map[K]V, l.Len())
	for k, v := range l.m {
		m[k] = v.Value.value
	}
	return m
}

func (l *LinkedHashMap[K, V]) Clear() {
	var mapSize = 100
	if l.initialCapacity <= 0 {
		mapSize = l.initialCapacity
	}
	l.m = make(map[K]*Element[mapEntry[K, V]], mapSize)
	l.l = List[mapEntry[K, V]]{}
}

func (l *LinkedHashMap[K, V]) Keys() []K {
	keys := make([]K, 0, len(l.m))
	if l.l.Len() == 0 {
		return keys
	}
	for cur := l.l.root.next; cur != &l.l.root; cur = cur.next {
		keys = append(keys, cur.Value.key)
	}
	return keys
}

func (l *LinkedHashMap[K, V]) Len() int {
	return len(l.m)
}

func (l *LinkedHashMap[K, V]) Size() int {
	return l.Len()
}

func (l *LinkedHashMap[K, V]) IsEmpty() bool {
	return len(l.m) == 0
}

// Puts the new key-value pair into the structure.  Returns any
// pre-existing value that was at this key.
func (l *LinkedHashMap[K, V]) Put(key K, value V) V {
	newMapEntry := mapEntry[K, V]{key: key, value: value}
	oldElement, found := l.m[key]
	var oldValue mapEntry[K, V]
	if found {
		oldValue = l.l.Remove(oldElement)
	}
	newElement := l.l.PushFront(newMapEntry)
	l.m[key] = newElement

	return oldValue.value
}

func (l *LinkedHashMap[K, V]) Get(key K) (V, error) {
	element, found := l.m[key]
	if !found {
		return *new(V), ErrNotFound
	}
	l.l.MoveToFront(element)
	return element.Value.value, nil
}

func (l *LinkedHashMap[K, V]) GetNoReset(key K) (V, error) {
	element, found := l.m[key]
	if !found {
		return *new(V), ErrNotFound
	}
	return element.Value.value, nil
}

func (l *LinkedHashMap[K, V]) GetFirst() V {
	element := l.l.Front()
	return element.Value.value
}

func (l *LinkedHashMap[K, V]) GetLast() V {
	element := l.l.Back()
	return element.Value.value
}

func (l *LinkedHashMap[K, V]) Remove(key K) (V, error) {
	element, found := l.m[key]
	if found {
		delete(l.m, key)
		l.l.Remove(element)
		return element.Value.value, nil
	}
	return *new(V), ErrNotFound
}

func (l *LinkedHashMap[K, V]) RemoveTail() V {
	element := l.l.Back()
	if element != nil {
		delete(l.m, element.Value.key)
		l.l.remove(element)
		return element.Value.value
	}
	return *new(V)
}
